# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState
from isiguardianlib.sensor_correction import states as sc_states
from isiguardianlib import const as top_const
import HAM_const as const
######################################

SC_ramp_time = 1

##################################################
# Configuration Control

class desired_conf():
    """This will make it a bit more clear what configuration and filter 
    we are calling when writing new states. The purpose was to allow for 
    easy state creation. Hopefully it makes it easier and not more 
    confusing..

    Sample use:
    >>> A = desired_conf('SC', 'A')
    >>> A.Filter_Banks
    ['WNR','FIR']
    >>> A.MATCH_X
    [2,3]
    """
    def __init__(self, BLENDorSC, conf_letter):
        if BLENDorSC == 'BLEND':
            self.blend = True
            self.SC = False
            self.filter_dict = const.BLEND_FILTER_SELECTION
        elif BLENDorSC == 'SC':
            self.SC = True
            self.blend = False
            self.filter_dict = const.SC_FM_confs
        self.conf = conf_letter
        # Run to set attributes
        self.bank_dof()
    @property
    def Filter_Banks(self):
        banks = [bank for bank in self.filter_dict[self.conf]]
        final_banks = []
        for bank in banks:
            total_dofs = []
            for dof in self.filter_dict[self.conf][bank]:
                if self.filter_dict[self.conf][bank][dof]:
                    total_dofs += dof
            if len(total_dofs) != 0:
                final_banks.append(bank)
        return final_banks
    @property
    def Blend_dofs(self):
        if self.blend:
            return [dof for dof in self.filter_dict[self.conf]['BLEND'] \
                        if self.filter_dict[self.conf]['BLEND'][dof]]
        else:
            return None
    @property
    def SC_dofs(self):
        if self.SC:
            SCdict = {}
            for bank in self.Filter_Banks:
                SCdict[bank] = [dof for dof in self.filter_dict[self.conf][bank] \
                        if self.filter_dict[self.conf][bank][dof]]
            return SCdict
        else:
            return None
    def bank_dof(self):
        for bank in self.Filter_Banks:
            for dof in self.filter_dict[self.conf][bank]:
                setattr(self, bank+'_'+dof, self.filter_dict[self.conf][bank][dof])

A_SC = desired_conf('SC', 'A')
B_SC = desired_conf('SC', 'B')
##################################################
# States

class INIT(GuardState):
    """Check the status of the blends and SC to determine which state to go to"""
    def main(self):
        dof_states = []
        # FIXME: works, but could be better, see note below.
        # This assumes that we are changing all the same dofs for each
        # SC config. If that ever changes, then this will no longer work
        for dof in A_SC.SC_dofs[A_SC.Filter_Banks[0]]:
            WNR_gain = ezca['SENSCOR_GND_STS_{}_WNR_GAIN'.format(dof)]
            FIR_gain = ezca['SENSCOR_GND_STS_{}_FIR_GAIN'.format(dof)]
            if WNR_gain == 1.0 and FIR_gain == 0.0:
                dof_states.append('SC_A')
            elif WNR_gain == 0.0 and FIR_gain == 1.0:
                dof_states.append('SC_B')
            elif WNR_gain == 0.0 and FIR_gain == 0.0:
                dof_states.append('SC_OFF')
            else:
                dof_states.append('DOWN')
        if all(sug == dof_states[0] for sug in dof_states):
            return dof_states[0]
        else:
            log('No known state found, going to DOWN')
            return 'DOWN'

####################
# SC states

TURN_ON_SC_A = sc_states.get_SC_switch_one_bank(
    A_SC.Filter_Banks[0],
    A_SC.SC_dofs[A_SC.Filter_Banks[0]],
    1,
    ramp_time=1)

TURN_ON_SC_B = sc_states.get_SC_switch_one_bank(
    B_SC.Filter_Banks[0],
    B_SC.SC_dofs[B_SC.Filter_Banks[0]],
    1,
    ramp_time=1)

### FIXME: This only works while we are just using one FB, need to find a more creative way to find the dofs
TURN_OFF_SC = sc_states.get_SC_all_gains_off(
    A_SC.Filter_Banks + B_SC.Filter_Banks,
    const.SC_ENGAGED_DOF,
    ramp_time=1)

####################
# Idle states

def get_gen_idle_state(index):
    class IDLE_STATE(GuardState):
        def main(self):
            return True
        def run(self):
            return True
    IDLE_STATE.index = index
    return IDLE_STATE

SC_B = get_gen_idle_state(30)
SC_A = get_gen_idle_state(20)
SC_OFF = get_gen_idle_state(10)



##########
class DOWN(GuardState):
    """This state is more of just a parking spot for the node to go
    when someone wants to change the configuration to something other
    than what is defined here, or if there is a problem it can jump to DOWN.
    """
    goto = True
    index = 1
    def main(self):
        return True
    def run(self):
        return True
##################################################

edges = [
    ('SC_OFF', 'TURN_ON_SC_A'),
    ('TURN_ON_SC_A', 'SC_A'),
    ('SC_A', 'TURN_OFF_SC'),

    ('TURN_OFF_SC', 'SC_OFF'),

    ('SC_OFF', 'TURN_ON_SC_B'),
    ('TURN_ON_SC_B', 'SC_B'),
    ('SC_B', 'TURN_OFF_SC'),
    ]

